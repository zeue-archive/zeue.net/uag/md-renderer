<?php

$bn = basename("https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");

if ($bn == "md") {
    echo "<title>uag's md renderer</title>";
	echo "no file selected";
} else {
	$filePlace = "md/".$bn.".md";
	if (!file_exists($filePlace)) {
        echo "<title>error: 404</title>";
		echo "<pre style='background:red;color:white;text-align:center;font-weight:bold;'>file requested: ".$bn." (unavailable)</pre>";
	} else {
        echo "<title>".$bn."</title>";
    
		include("inc/Parsedown.php");
		
		echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
		
		echo "<style>".file_get_contents("inc/github-style-markdown.css")."</style>";
		
		echo "<pre style='background:lime;color:black;text-align:center;font-weight:bold;'>file requested: ".$bn." (available)</pre>";
		
		echo "<article class='markdown-body'>";
		$contents = file_get_contents($filePlace);
		$Parsedown = new Parsedown();
		echo $Parsedown->text($contents);
		echo "</article>";
	}
}
