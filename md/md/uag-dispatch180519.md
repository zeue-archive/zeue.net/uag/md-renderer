# **DISPATCH:** `Don't Fear The Dusty Reaper`
## **Terrain:** `Takistan`

<a href='https://postimg.cc/image/j4rfs65ij/' target='_blank'><img src='https://s9.postimg.cc/j4rfs65ij/xp6917w24fjy.jpg' border='0' alt='xp6917w24fjy'/></a>

### **Situation**

`We have been tasked with quelling the recent militia uprising in Takistan, by any means necessary.`

**BLUFOR**

`Wolfpack and Hammer`

<a href='https://postimg.cc/image/6pxhsbe7f/' target='_blank'><img src='https://s9.postimg.cc/6pxhsbe7f/Stryker_MCV-_B.jpg' border='0' alt='Stryker_MCV-_B'/></a>

**REDFOR**

`Islamic State of Takistan`

<a href='https://postimg.cc/image/9wz7bsqu3/' target='_blank'><img src='https://s9.postimg.cc/9wz7bsqu3/2014_11_05_00004_4.jpg' border='0' alt='2014_11_05_00004_4'/></a>

**GREENFOR**

`Any other combat elements resisting the local government`

### **Mission**

- `Follow the patrol route and neutralise any and all resistance`

### **Execution**

**Commander's Intent**

`Effectively supress the locals' attempts at an uprising, by any means necessary`

**Movement Plan**

`Light mechanised and wheeled vehicles are permitted, depending on number of contractors`

### **Extra Information**

`None`