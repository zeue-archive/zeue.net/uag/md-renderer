> This program is currently not enforced or supported

# **UAG Recruitment Incentive Program**

### Get paid for recruiting for UAG!

## Abstract

The intention of this program is to ensure UAG (Unnamed Arma Group) has a healthy amount of recruits at all times to ensure proper growth of the unit. We work very hard on delivering an immersive experience and currently lack a HR division, so it is hard for us to find the time to manage the recruitment aspect of running a unit. This has lead us to devise a better way of managing this problem, by outsourcing it with our leftover finances!

## How it works

We've tried to make the process as simple as possible, and here it is:

- You will get **$5** for every **new recruit** who makes it through the **2 week trial**
- You will get an extra **$10** for every **member** that stays for **3 months**

## How to sign up

Feel free to contact Cody using any of the following methods:

**Discord:** zeue#0001 (or by accepting [this invite](https://armapmc.com/discord))

**Email:** 5887963@gmail.com

