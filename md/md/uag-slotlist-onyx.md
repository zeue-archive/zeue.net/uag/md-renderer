> This project is heavily under development by UAG's NCG:Tech and does not consider it ready for production use, **you have been warned.**

# SlotList Onyx

## UAG's Fork of [SlotList](https://github.com/MorpheusXAUT/slotlist-frontend) by [MorpheusXAUT](https://github.com/MorpheusXAUT)

This page is still under construction, for now here's a list of stuff we're working on:

- Integration with Google Cloud Platform's SDK
- Add support for TeamSpeak servers and Discord invites
- Allow ongoing missions to be viewed as live feedback to the slotlist frontend
- Use of a server-side addon to generate comprehensive AAR's
- Integrate NCG:Tech's client-side statistics module to allow admins to review per-contractor performance
- New admin interface for slotlist, based on armapmc's modular design philosophy
- Use of the slotlist backend to control GCP servers by scheduling server spinups and configurations based on slotlist entries